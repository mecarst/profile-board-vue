import Vue from 'vue';
import VueRouter from 'vue-router';

import AppHome from '../views/Home.vue';
import AppUser from '../views/User.vue';
import AppError from '../views/Error.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', name: 'home', component: AppHome },
  { path: '/user/:id', name: 'user', component: AppUser },
  { path: '*', component: AppError }
]

export default new VueRouter({
  routes: routes,
  mode: 'history'
})