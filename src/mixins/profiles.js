import axios from 'axios';

import { EventBus } from '../utils/index.js';

export default {
  data() {
    return {
      profiles: []
    }
  },
  methods: {
    requestProfiles() {
      return axios
        .get('https://profiles-list.firebaseio.com/Data.json')
        .then(res => this.profiles = res.data.splice(0, 100))
        .then(() => {
          EventBus.$emit('profiles-loaded', this.profiles);
        });
    },
    getProfileById(id) {
      EventBus.$on('profiles-loaded', profiles => {
        profiles.filter(profile => {
          if (profile.localid === parseInt(id)) {
            EventBus.$emit('profile-loaded', profile);
          }
        })
      });
    },
    searchProfile(searchTerm) {
      let filteredProfiles = [];
      
      this.profiles.filter(profile => {
        for (let prop in profile) {
          if (profile[prop].toString().toLowerCase().indexOf(searchTerm) >= 0) {
            filteredProfiles.push(profile);
          }
        }
      });

      if (filteredProfiles.length > 0) {
        this.profiles = filteredProfiles;
      }
    }
  },
  beforeMount() {
    this.requestProfiles();
  },
}